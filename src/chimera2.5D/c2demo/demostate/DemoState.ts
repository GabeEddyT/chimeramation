import {TextRenderer} from '../../c2graphics/TextRenderer'
export enum ObjectMaxCounts{
    demoStateMaxCount_object = 8,
    demoStateMaxCount_camera = 1,
    demoStateMaxCount_light = 1,
    demoStateMaxCount_sceneObject = demoStateMaxCount_object + demoStateMaxCount_camera + demoStateMaxCount_light,

    demoStateMaxCount_timer = 1,
    demoStateMaxCount_drawDataBuffer = 8,
    demoStateMaxCount_vertexArray = 12,
    demoStateMaxCount_drawable = 8,
    demoStateMaxCount_shaderProgram = 8,
};

export enum DemoStateModeCounts{
    demoStateMaxModes = 1,
    demoStateMaxSubModes = 1,
    demoStateMaxOutputModes = 1,
};

export class DemoState{
    // general variables pertinent to the state

		// terminate key pressed
		exitFlag: boolean;

		// global vertical axis: Z = 0, Y = 1
		verticalAxis: number;

		// asset streaming between loads enabled (careful!)
		streaming: boolean;

		// window and full-frame dimensions
        windowWidth: number;
        windowHeight: number;
        frameWidth: number; 
        frameHeight: number;
		frameBorder: number;
        

		//---------------------------------------------------------------------
		// objects that have known or fixed instance count in the whole demo
        
		// text renderer
        textInit: number;
        textMode: number;
        textModeCount: number;
		text: TextRenderer;

		// input
		// mouse : {
		// 	btn: any, btn0: {
		// 		btn: Uint8Array;
		// 	};
		// };
		// a3_KeyboardInput keyboard[1];
		// a3_XboxControllerInput xcontrol[4];

		// pointer to fast trig table
		trigTable = new Float32Array (4096 * 4);


		//---------------------------------------------------------------------
		// scene variables and objects

		// demo mode array: 
		//	- mode (1): which mode/pipeline is being viewed
		//	- sub-mode (1): which sub-mode/pass in the pipeline is being viewed
		//	- output (1): which output from the sub-mode/pass is being viewed
		demoMode: number; demoSubMode = new Uint32Array(DemoStateModeCounts.demoStateMaxModes); demoOutputMode: number[][] = [[DemoStateModeCounts.demoStateMaxModes], [DemoStateModeCounts.demoStateMaxSubModes]];
		demoModeCount: number; demoSubModeCount: number[] = [DemoStateModeCounts.demoStateMaxModes]; demoOutputCount:number[][] = [[DemoStateModeCounts.demoStateMaxModes], [DemoStateModeCounts.demoStateMaxSubModes]];

		// toggle grid in scene and axes superimposed
		displayGrid: number; displaySkybox: number; displayWorldAxes: number; displayObjectAxes: number;
		updateAnimation: number;

		// grid properties
		a3mat4 gridTransform;
		a3vec4 gridColor;

		// cameras
		a3ui32 activeCamera;

		// lights
		a3_DemoPointLight pointLight[demoStateMaxCount_light];
		a3mat4 pointLightMVP[demoStateMaxCount_light];
		a3ui32 lightCount;


		//---------------------------------------------------------------------
		// object arrays: organized as anonymous unions for two reasons: 
		//	1. easy to manage entire sets of the same type of object using the 
		//		array component
		//	2. at the same time, variables are named pointers

		// scene objects
		union {
			a3_DemoSceneObject sceneObject[demoStateMaxCount_sceneObject];
			struct {
				a3_DemoSceneObject
					cameraObject[demoStateMaxCount_camera],	// transform for cameras
					lightObject[demoStateMaxCount_light],	// transform for lights
					object[demoStateMaxCount_object];		// transform for general objects
			};
			struct {
				a3_DemoSceneObject
					mainCamera[1],

					mainLight[1],
					
					skyboxObject[1],
					planeObject[1],
					sphereObject[1],
					cylinderObject[1],
					torusObject[1],
					teapotObject[1];
			};
		};

		// cameras
		//	- any object can have a camera "component"
		union {
			a3_DemoCamera camera[demoStateMaxCount_camera];
			struct {
				a3_DemoCamera
					sceneCamera[1];						// scene viewing cameras
			};
		};

		// timers
		union {
			a3_Timer timer[demoStateMaxCount_timer];
			struct {
				a3_Timer
					renderTimer[1];						// render FPS timer
			};
		};


		// draw data buffers
		union {
			a3_VertexBuffer drawDataBuffer[demoStateMaxCount_drawDataBuffer];
			struct {
				a3_VertexBuffer
					vbo_staticSceneObjectDrawBuffer[1];			// buffer to hold all data for static scene objects (e.g. grid)
				a3_VertexBuffer
					vbo_planeDrawBuffer[1],						//dedicated buffer for plane object
					vbo_sphereDrawBuffer[1],					//dedicated buffer for sphere object
					vbo_cylinderDrawBuffer[1],					//dedicated buffer for cylinder object
					vbo_torusDrawBuffer[1],						//dedicated buffer for torus object
					vbo_teapotDrawBuffer[1];					//dedicated buffer for teapot object
			};
		};

		// vertex array objects
		union {
			a3_VertexArrayDescriptor vertexArray[demoStateMaxCount_vertexArray];
			struct {
				a3_VertexArrayDescriptor
					vao_position[1],							// VAO for vertex format with only position
					vao_position_color[1],						// VAO for vertex format with position and color
					vao_position_texcoord[1],					// VAO for vertex format with position and UVs
					vao_position_texcoord_normal[1];			// VAO for vertex format with position, UVs and normal
				a3_VertexArrayDescriptor
					vao_planeFormat[1],							//dedicated format for plane object
					vao_sphereFormat[1],						//dedicated format for sphere object
					vao_cylinderFormat[1],						//dedicated format for cylinder object
					vao_torusFormat[1],							//dedicated format for torus object
					vao_teapotFormat[1];						//dedicated format for teapot object
			};
		};

		// drawables
		union {
			a3_VertexDrawable drawable[demoStateMaxCount_drawable];
			struct {
				a3_VertexDrawable
					draw_grid[1],								// wireframe ground plane to emphasize scaling
					draw_axes[1],								// coordinate axes at the center of the world
					draw_skybox[1],								// skybox cube mesh
					draw_plane[1],								// procedural plane
					draw_sphere[1],								// procedural sphere
					draw_cylinder[1],							// procedural cylinder
					draw_torus[1],								// procedural torus
					draw_teapot[1];								// can't not have a Utah teapot
			};
		};


		// shader programs and uniforms
		union {
			a3_DemoStateShaderProgram shaderProgram[demoStateMaxCount_shaderProgram];
			struct {
				a3_DemoStateShaderProgram
					prog_drawColorUnif[1],						// draw uniform color
					prog_drawColorAttrib[1],					// draw color attribute
					prog_drawColorUnif_instanced[1],			// draw uniform color with instancing
					prog_drawColorAttrib_instanced[1];			// draw color attribute with instancing
			};
		};


		// managed objects, no touchie
		a3_VertexDrawable dummyDrawable[1];
};

let x = new DemoState;
x.demoOutputMode[1][2] = 0