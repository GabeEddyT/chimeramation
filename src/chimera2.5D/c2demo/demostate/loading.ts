import * as c2 from "../../chimera2.5D"
import passthru_transform_vs from "../glsl/vs/passThruTransform.glsl"
import passColor_transform_vs from "../glsl/vs/passColorTransform.glsl"
import drawColorUnif_fs from "../glsl/fs/drawColorUnif.glsl";
import drawColorAttrib_fs from "../glsl/fs/drawColorAttrib.glsl"


const gl = c2.gl;

function LoadGeometry(demostate: any){
    const downscale20x = [
        +0.05, 0.0, 0.0, 0.0,
		0.0, +0.05, 0.0, 0.0,
		0.0, 0.0, +0.05, 0.0,
		0.0, 0.0, 0.0, +1.0,
    ];

    const positions = [
        // Front face
        -1.0, -1.0,  1.0,
         1.0, -1.0,  1.0,
         1.0,  1.0,  1.0,
        -1.0,  1.0,  1.0,
        
        // Back face
        -1.0, -1.0, -1.0,
        -1.0,  1.0, -1.0,
         1.0,  1.0, -1.0,
         1.0, -1.0, -1.0,
        
        // Top face
        -1.0,  1.0, -1.0,
        -1.0,  1.0,  1.0,
         1.0,  1.0,  1.0,
         1.0,  1.0, -1.0,
        
        // Bottom face
        -1.0, -1.0, -1.0,
         1.0, -1.0, -1.0,
         1.0, -1.0,  1.0,
        -1.0, -1.0,  1.0,
        
        // Right face
         1.0, -1.0, -1.0,
         1.0,  1.0, -1.0,
         1.0,  1.0,  1.0,
         1.0, -1.0,  1.0,
        
        // Left face
        -1.0, -1.0, -1.0,
        -1.0, -1.0,  1.0,
        -1.0,  1.0,  1.0,
        -1.0,  1.0, -1.0,
    ];


    const numComponents = 3;

    let vbo_ibo;
    let vao;
    let currentDrawable;
    let sharedVertexStoreage = 0, sharedIndexStorage = 0;
    let numVerts = 0;

    const prog_drawColorUnif = initShaderProgram(passthru_transform_vs, drawColorUnif_fs);
    const prog_drawColorAttrib = initShaderProgram(passColor_transform_vs, drawColorAttrib_fs);

    const programInfo = {
        program: prog_drawColorUnif,
        attribLocations: {
          vertexPosition: gl.getAttribLocation(prog_drawColorUnif, 'aPosition'),
          vertexColor: gl.getAttribLocation(prog_drawColorUnif, 'aColor'),
        },
        uniformLocations: {
          projectionMatrix: gl.getUniformLocation(prog_drawColorUnif, 'uP'),
          modelViewMatrix: gl.getUniformLocation(prog_drawColorUnif, 'uMVP'),
        },
    };
}


function initShaderProgram(vsSource, fsSource) {
    const vertexShader = loadShader(gl.VERTEX_SHADER, vsSource);
    const fragmentShader = loadShader(gl.FRAGMENT_SHADER, fsSource);
  
    // Create the shader program
  
    const shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);
  
    // If creating the shader program failed, alert
  
    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
      alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
      return null;
    }
  
    return shaderProgram;
  }
  
  //
  // creates a shader of the given type, uploads the source and
  // compiles it.
  //
  function loadShader(type, source) {
    const shader = gl.createShader(type);
  
    // Send the source to the shader object
  
    gl.shaderSource(shader, source);
  
    // Compile the shader program
  
    gl.compileShader(shader);
  
    // See if it compiled successfully
  
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
      gl.deleteShader(shader);
      return null;
    }
  
    return shader;
  }