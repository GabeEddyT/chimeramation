import c2 from "../chimera2.5D"
import { GraphicsObjectHandle } from "./GraphicsObjectHandle";
import { PixelFormatDescriptor } from "./Texture";

/** Absolute maximum number of color targets allowed. */
export const colorTargetMax = 16;

/**
    Framebuffer structure used for off-screen rendering.
	@member handle: graphics object handle
	@member colorTextureHandle: list of graphics handles for color targets
	@member depthTextureHandle: graphics handle for depth target
	@members frameWidth, frameHeight: dimensions of frame buffer in pixels
	@member color: number of color targets attached to this framebuffer
    @member depthStencil: depth buffer format
*/
export class Framebuffer{
    handle: GraphicsObjectHandle;
    colorTextureHandle: number;
    depthTextureHandle: number;
    frameWidth: number; 
    frameHeight: number;
    color: number; 
    depthStencil: number;
};

/**
    Paired framebuffers for off-screen double-buffering.
 	@member handle: graphics object handle
 	@member handleDouble: graphics object handle for double buffer
 	@member colorTextureHandle: list of graphics handles for color targets
	@member depthTextureHandle: graphics handles for depth targets
	@members frontColor, frontDepth: color texture offsets for front buffer
	@members frameWidth, frameHeight: dimensions of frame buffers in pixels
	@member color: number of color targets attached to each framebuffer
    @member depthStencil: depth buffer format
*/
export class FramebufferDouble{
    handle: GraphicsObjectHandle;
    handleDouble: number;
    colorTextureHandle: number;
    depthTextureHandle: number;
    frontColor: number;
    frontDepth: number;
    frameWidth: number;
    frameHeight: number;	
    color: number;
    depthStencil: number;
};

/** High-level descriptor of color format for framebuffers. */
export enum ColorType{
    Disable,				// no color targets attached
    RGB8 = c2.tex_pt.rgb8,	// 3 channels, 8 bits each (byte)
    RGB16,				// 3 channels, 16 bits each (short)
    RGB32F,				// 3 channels, 32 bits each (float)
    RGBA8,				// 4 channels, 8 bits each
    RGBA16,				// 4 channels, 16 bits each
    RGBA32F,				// 4 channels, 32 bits each (float)
};

/** High-level descriptor of depth format for framebuffers. */
export enum DepthType
{
    depthDisable,				// no depth target
    depth16 = c2.tex_pt.depth16,	// 16-bit depth (65536 possible values)
    depth24,					// 24-bit depth (16 million values)
    depth32,					// 32-bit depth (4 billion values)
    depth24_stencil8,			// 24-bit depth with 8-bit stencil
};

/** Create framebuffer.
    @param framebuffer_out non-null pointer to uninitialized framebuffer
    @param name_opt optional cstring for short name/description; max 31 
    	chars + null terminator; pass null for default name
    @param colorTargets number of color targets (relevant if colorType is 
    	not 'disable')
    @param colorType color type to use for render targets (relevant if 
    	colorTargets is not zero)
    @param depthType depth type to use
    @param frameWidth framebuffer width
    @param frameHeight framebuffer height
    @return 1 if success
    @return 0 if failed
    @return -1 if invalid params (null pointer or already initialized)
*/
/*
export function Create(framebuffer_out: [Framebuffer], name_opt: string, colorTargets: number, colorType: ColorType, depthType: DepthType, frameWidth: number, frameHeight: number){
    let ret: Framebuffer;
	let colorPixelFormat: PixelFormatDescriptor, depthPixelFormat: PixelFormatDescriptor;
	let handle: GraphicsObjectHandle, numHandles: number;

	// validate params
	if (framebuffer_out && frameWidth && frameHeight)
	{
		// validate unused
		if (framebuffer_out[0].handle.handle)
		{
			// get pixel formats
			if (colorType && colorTargets)
			{
				colorTargets = InternalValidateColorTargets(colorTargets);
				a3textureCreatePixelFormatDescriptor(colorPixelFormat, (a3_TexturePixelType)colorType);
				colorPixelFormatPtr = colorPixelFormat;
			}
			if (depthType)
			{
				a3textureCreatePixelFormatDescriptor(depthPixelFormat, (a3_TexturePixelType)depthType);
				depthPixelFormatPtr = depthPixelFormat;
			}

			// prepare FBO
			handle = a3framebufferInternalCreate(ret.colorTextureHandle, ret.depthTextureHandle, 
				colorTargets, colorPixelFormatPtr, depthPixelFormatPtr, frameWidth, frameHeight);
			if (handle)
			{
				// initialize graphics object, copy data and return
				numHandles = (2 + a3fbo_colorTargetMax);
				a3handleCreateHandle(ret.handle, a3framebufferInternalHandleReleaseFunc, name_opt, handle, numHandles);
				ret.color = (colorType != a3fbo_colorDisable) * colorTargets;
				ret.depthStencil = depthType;
				ret.frameWidth = frameWidth;
				ret.frameHeight = frameHeight;

				// done
				*framebuffer_out = ret;
				a3framebufferReference(framebuffer_out);
				return 1;
			}
			else
				printf("\n A3 ERROR (FBO \'%s\'): \n\t Invalid handle; framebuffer not created.", name_opt);

			// fail
			return 0;
		}
	}
	return -1;
}
*/

// WebGL

const gl = c2.gl;

// release functions
// export function InternalHandleReleaseFunc(count: number, handle: WebGLBuffer)
// {
// 	// first is framebuffer
//     // the rest are textures
//     gl.deleteFramebuffer(handle);
//     gl.deleteTexture()
//     gl.IL_
// 	glDeleteTextures(count -= 1, handle += 1);
// }

