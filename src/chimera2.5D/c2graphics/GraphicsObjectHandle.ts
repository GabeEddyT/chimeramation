// header

/**
	Generic reference-counting graphics object handle.
	@member releaseFunc: function pointer to auto-release function
	@member name: short name/description of handle, useful for debugging; 
		max 31 characters + null terminator (adds terminator on create)
	@member objectCount: how many objects were initialized under this handle
	@member refCount: reference counter for this handle (i.e. the option to 
		keep track of how many other handles are dependent on this one)
	@members handle, handlePtr: overlapping handle used by OpenGL, 
		represented as both an integer and a pointer (because array)
	@NOTE because the handle occurs at the end of the structure, any other 
		data type that declares a graphics handle instance may declare an 
		array of integers immediately following to store multiple handles; 
		e.g. a struct that has 4 graphics objects of the same type may 
		declare its handles like this: 
			a3_GraphicsObjectHandle handle[1];
			a3ui32 handles_extra[3];
*/
export class GraphicsObjectHandle
{
    releaseFunc: Function;
    name: string;
    objectCount: number;
    refCount: number;
	handle: any; 
	
	/**
	Create generic handle with an custom object release function.
	@param handle_out non-null pointer to uninitialized graphics object 
		handle container
	@param releaseFunc_opt optional pointer to release function with 
		return type void, and WebGLBuffer argument
	@param name_opt optional cstring for short name/description; max 31 
		chars + null terminator; pass null for default name
	@param handleValue non-zero initial value of handle
	@param objectCount number of handles to be released (not always used)
	@return handleValue if successfully created
	@return -1 if invalid params or if already in-use
*/
	CreateHandle(releaseFunc_opt: Function, name_opt: string, handleValue: WebGLBuffer, objectCount: number)
	{
		// if valid
		if (handleValue)
		{
			// if not already in-use
			if (!this.handle)
			{
				this.releaseFunc = releaseFunc_opt;
				this.objectCount = objectCount;
				this.refCount = 0;
				this.SetName(name_opt);
				return (this.handle = handleValue);
			}
		}
		return -1;
	}
	/**
		Update handle delete callback with custom function.
		@param handle non-null pointer to graphics object handle container
		@param releaseFunc_opt optional pointer to release function with 
			return type void and WebGLBuffer argument
		@return 1 if successfully updated
		@return -1 if invalid params
	*/
	SetReleaseFunc(releaseFunc_opt: Function)
	{
		if (this)
		{
			this.releaseFunc = releaseFunc_opt;
			return 1;
		}
		return -1;
	}

	/**
		Increment handle counter.
		@param handle non-null pointer to graphics object handle container
		@return new count if success
		@return -1 if invalid param or internal handle is zero
	*/
	IncrementCount()
	{
		// if valid, increment count and return
		if (this.handle)
			return ++(this.refCount);
		return -1;
	}

	/**
		Decrement handle counter; calls release function if one is provided 
			and count hits zero
		@param handle non-null pointer to graphics object handle container
		@return new count (min 0) if success
		@return -1 if invalid param or internal handle is zero
	*/
	DecrementCount()
	{
		// if valid
		if (this && this.handle)
		{
			// decrement count; if invalidated, release
			if (--(this.refCount) <= 0)
				return this.Release();
			return this.refCount;
		}
		return -1;
	}

	/**
		Force-release handle; set value to zero and call release function.
		@param handle non-null pointer to graphics object handle container
		@return 0 if success
		@return -1 if invalid param or internal handle is zero
	*/
	Release()
	{
		// if valid
		if (this && this.handle)
		{
			if (this.releaseFunc)
				this.releaseFunc(this.handle);

			// reset
			this.releaseFunc = null;
			this.objectCount = 0;
			this.refCount = 0;
			this.handle = null;
			return 0;
		}
		return -1;
	}

	// standard

	static handleNameCtr = 0;

	/**
		Set the name of a handle.
		@param handle non-null pointer to graphics object handle container
		@param name_opt optional cstring for short name/description; pass null for default name
		@return 1 if success
		@return -1 if invalid params
	*/
	SetName(name_opt?: string)
	{
		if (this)
		{
			if (name_opt)
			{
				this.name = name_opt;
			}
			else
			{
				this.name = "a3handle_";
				this.name += ++GraphicsObjectHandle.handleNameCtr;
			}
			return 1;
		}
		return -1;
	}
};


// inlines











