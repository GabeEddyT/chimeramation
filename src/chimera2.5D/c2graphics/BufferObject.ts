import c2 from "../chimera2.5D";
import { gl } from "../chimera2.5D";
import { GraphicsObjectHandle } from "./GraphicsObjectHandle";

// header

/**
	Enumeration of buffer data types.
	@member vertex: store vertex data in a "vertex buffer object" (VBO)
	@member index: store index data in an "index buffer object" (IBO), 
		a.k.a. "element buffer object" (EBO)
	@member uniform: store uniform data in a "uniform buffer object" (UBO)
	@NOTE It is also possible for vertex and index data to coexist in the 
		same buffer; use either a3buffer_vertex or a3buffer_index mode to 
		achieve this; avoid using 'deactivate' function for shared buffer.
*/
export enum Type{
	vertex= 0,
	index,
	uniform
};

export class BufferObject{
	handle: GraphicsObjectHandle;
	type: Type;
	internalBinding: number;
	size: number;
	split: [number, number] = [0, 0];
	used: [number, number] = [0, 0];	


	// inlines
	
	ValidateBlockSize (section: number, size: number){
		section = section ? 1 : 0;
		if (this.handle.handle && size)
			return ((this.used[section] + size) <= this.split[section]);
		return -1;
	}

	GetCurrentOffset(section: number){
		if (this.handle.handle && this.size)
			return (this.used[section ? 1 : 0]);
		return -1;
	}

	Reference()	{		
		return this.handle.IncrementCount();		
	}

	Release()	{
		const ret = this.handle.DecrementCount();
		if (ret == 0)
			this.size = this.split[0] = this.split[1] = this.used[0] = this.used[1] = this.internalBinding = 0;
		return ret;
	}

	// standard

	InternalFlag(bufferType: Type){
		const bufferBindings = [gl.ARRAY_BUFFER, gl.ELEMENT_ARRAY_BUFFER, gl.UNIFORM_BUFFER];
		return bufferBindings[bufferType];
	}

	InternalFillHint(bufferType: Type){
		const bufferFillHint = [gl.STATIC_DRAW, gl.STATIC_DRAW, gl.DYNAMIC_DRAW];
		return bufferFillHint[bufferType];
	}

	InternalFillSub(binding: number, start: number, data: ArrayBuffer)
	{
		gl.bufferSubData(binding, start, data);
	}

	InternalReleaseFunc(handle: WebGLBuffer){
		gl.deleteBuffer(handle);
	}

	Create(name_opt: string, bufferType: Type, size: number, initialData_opt: any){
		let handle: WebGLBuffer;
		let binding: number;
		let fillHint: number;
		this.handle = new GraphicsObjectHandle;
		if (size)
		{
			// check uninitialized
			if (!this.handle.handle)
			{
				// generate buffer
				handle = gl.createBuffer();
				if (handle)
				{
					binding = this.InternalFlag(bufferType);
					fillHint = this.InternalFillHint(bufferType);
					
					// bind and allocate space
					gl.bindBuffer(binding, handle);				
					gl.bufferData(binding, initialData_opt, fillHint, 0, size);
					gl.bindBuffer(binding, null);
					// configure
					this.handle.CreateHandle(this.InternalReleaseFunc, name_opt, handle, 1);
					this.type = bufferType;
					this.internalBinding = binding;
					this.size = this.split[0] = this.split[1] = size;
					this.used[0] = this.used[1] = initialData_opt ? size : 0;
					// done, copy output
					
					this.Reference();
	
					return 1;
				}
				else
					console.log("\n A3 ERROR (BUF '%s'): \n\t Invalid handle; buffer not created.", name_opt);
	
				// fail
				return 0;
			}
		}
		return -1;
	}

	CreateSplit(name_opt : string, bufferType : Type, size0: number, size1: number, initialData0_opt: any, initialData1_opt: any){
		let handle: WebGLBuffer;
		let binding: number;
		let fillHint: number;
		let size = size0 + size1;

		if (this && size)
		{
			// check uninitialized
			if (!this.handle.handle)
			{
				// generate buffer
				handle = gl.createBuffer();
				if (handle)
				{
					binding = this.InternalFlag(bufferType);
					fillHint = this.InternalFillHint(bufferType);

					// bind and allocate space
					gl.bindBuffer(binding, handle);
					gl.bufferData(binding, size, fillHint);
					gl.bufferSubData(binding, 0, initialData0_opt);
					gl.bufferSubData(binding, size0, initialData1_opt, 0, size1);
					gl.bindBuffer(binding, 0);

					// configure
					this.handle.CreateHandle(this.InternalReleaseFunc, name_opt, handle, 1);
					this.type = bufferType;
					this.internalBinding = binding;
					this.size = this.split[1] = size;
					this.split[0] = size0;
					this.used[0] = initialData0_opt ? size0 : 0;
					this.used[1] = initialData1_opt ? size : size0;


					// done, copy output
					
					this.Reference();
					return 1;
				}
				else
					console.log("\n A3 ERROR (BUF '%s'): \n\t Invalid handle; buffer not created.", name_opt);

				// fail
				return 0;
			}
		}
		return -1;
	}

	Fill(section: number, size: number, data: any, start_out_opt: [number])
	{
		let bHandle: number, start: number, end: number;

		if (this && size)
		{
			bHandle = this.handle.handle;
			if (bHandle)
			{
				// check if data will fit
				section = section ? 1 : 0;
				start = this.used[section];
				end = start + size;
				if (end <= this.split[section])
				{
					// bind and fill
					gl.bindBuffer(this.internalBinding, bHandle);
					gl.bufferSubData(this.internalBinding, start, data, 0, size);
					gl.bindBuffer(this.internalBinding, 0);

					// output starting point
					if (start_out_opt)
						start_out_opt[0] = start;

					// determine new tail
					this.used[section] = end;

					// done
					return size;
				}
				else
					console.log("\n A3 ERROR (BUF %o '%s'): \n\t Insufficient storage; failed to store data.", this.handle.handle, this.handle.name);

				// fail
				return 0;
			}
		}
		return -1;
	}

	FillOffset(section: number, offset: number, size: number, data: any, start_out_opt: [number])
	{
		let bHandle: any, start: number, end: number;

		if (this && size)
		{
			bHandle = this.handle.handle;
			if (bHandle)
			{
				// check if data will fit
				section = section ? 1 : 0;
				start = this.used[section] + offset;
				end = start + size;
				if (end <= this.split[section])
				{
					// bind and fill
					gl.bindBuffer(this.internalBinding, bHandle);
					gl.bufferSubData(this.internalBinding, start, data, 0, size);
					gl.bindBuffer(this.internalBinding, 0);

					// output starting point
					if (start_out_opt)
						start_out_opt[0] = start;

					// determine new tail
					if (end > this.used[section])
						this.used[section] = data ? end : start;

					// done
					return size;
				}
				else
					console.log("\n A3 ERROR (BUF %o '%s'): \n\t Insufficient storage; failed to store data.", this.handle.handle, this.handle.name);

				// fail
				return 0;
			}
		}
		return -1;
	}

	Activate(){
		if (this.handle.handle){
			gl.bindBuffer(this.internalBinding, this.handle.handle);
			return 1;
		}
		return -1;
	}

	DeactivateType(bufferType:Type){
		gl.bindBuffer(this.InternalFlag(bufferType), 0);
	}

	HandleUpdateReleaseCallback(){
		if (this)
			return this.handle.SetReleaseFunc (this.InternalReleaseFunc);
		return -1;
	}
}

export default BufferObject;