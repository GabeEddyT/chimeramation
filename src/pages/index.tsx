import React from "react"
import { Link } from "gatsby"

try{
  window.innerWidth;
}catch{
  global.window = {};
}

import * as twgl from  "twgl.js"

const OBJ = require("webgl-obj-loader");
const m4 = twgl.m4;
const v3 = twgl.v3;

import Layout from "../components/layout"
import Image from "../components/image"
// @ts-ignore
import teapot from "../chimera2.5D/c2demo/obj/teapot.obj.ts";

// Draw color attrib shader program
import passColorTransform from "../chimera2.5D/c2demo/glsl/vs/passColorTransform.glsl";
import drawColorAttrib from "../chimera2.5D/c2demo/glsl/fs/drawColorAttrib.glsl";

// Draw color uniform shader program
import passThruTransform from "../chimera2.5D/c2demo/glsl/vs/passThruTransform.glsl";
import drawColorUnif from "../chimera2.5D/c2demo/glsl/fs/drawColorUnif.glsl";

let buff: ArrayBuffer = new ArrayBuffer(4);
let arr = new Uint32Array (buff);

class IndexPage extends React.Component{ 
  static prevTime = 0;
  gl: WebGL2RenderingContext;
  canvas: HTMLCanvasElement;
  mounted: boolean;
  fov: number;
  progDrawColorUnif: any;
  cylinderBufferInfo: any;
  bufferInfo: any;
  teapotObj: any;
  teapotBufferInfo: twgl.BufferInfo;
  sphereBufferInfo: twgl.BufferInfo;
  planeBufferInfo: twgl.BufferInfo;
  cameraRot: number = 0;
  cameraRotUp: number = 5;
  mouseDown: boolean = false;
  dt: number;
  torusBufferInfo: twgl.BufferInfo;
  fps: number;
  fpsDiv: any;
  showFPS: boolean;
  showGrid: boolean;
  
  constructor(props){
    super(props);
    this.Update = this.Update.bind(this);
    this.Render = this.Render.bind(this);
    this.AdjustFov = this.AdjustFov.bind(this);
    this.HandleScroll = this.HandleScroll.bind(this);
    this.HandleDrag = this.HandleDrag.bind(this);
    this.HandleKeyPress = this.HandleKeyPress.bind(this);
    this.fpsDiv = React.createRef();
    this.showFPS = false;
    this.showGrid = true;
    this.mounted = false;
    this.fov = 80;
  }
  
  componentDidMount(){
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;

    window.addEventListener('wheel', this.HandleScroll);
    window.addEventListener('drag', this.HandleDrag);
    window.addEventListener('keypress', this.HandleKeyPress);

    this.gl = this.canvas.getContext("webgl2");
    const gl = this.gl;
    
    // cube example
    const arrays = {
      aPosition: [1,1,-1,1,1,1,1,-1,1,1,-1,-1,-1,1,1,-1,1,-1,-1,-1,-1,-1,-1,1,-1,1,1,1,1,1,1,1,-1,-1,1,-1,-1,-1,-1,1,-1,-1,1,-1,1,-1,-1,1,1,1,1,-1,1,1,-1,-1,1,1,-1,1,-1,1,-1,1,1,-1,1,-1,-1,-1,-1,-1],
      normal:   [1,0,0,1,0,0,1,0,0,1,0,0,-1,0,0,-1,0,0,-1,0,0,-1,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,-1,0,0,-1,0,0,-1,0,0,-1,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,-1,0,0,-1,0,0,-1,0,0,-1],
      texcoord: [1,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1],
      indices:  [0,1,2,0,2,3,4,5,6,4,6,7,8,9,10,8,10,11,12,13,14,12,14,15,16,17,18,16,18,19,20,21,22,20,22,23],
    };
    
    this.bufferInfo = twgl.createBufferInfoFromArrays(gl, arrays);  

    // cylinder
    this.progDrawColorUnif = twgl.createProgramInfo(gl, [passThruTransform, drawColorUnif]);
    this.cylinderBufferInfo = twgl.primitives.createCylinderBufferInfo(gl, 1.0, 3.0, 32, 1);
    this.cylinderBufferInfo.attribs.aPosition = this.cylinderBufferInfo.attribs.position;    
    
    // I'M A NOT-SO-LITTLE TEAPOT
    this.teapotObj = new OBJ.Mesh(teapot);
    const teapotObj = this.teapotObj;
    OBJ.initMeshBuffers(gl, teapotObj);

    const teapotArrays = {
      aPosition: teapotObj.vertices,
      indices: teapotObj.indices,
    }
    
    this.teapotBufferInfo = twgl.createBufferInfoFromArrays(gl, teapotArrays);    

    // sphere
    this.sphereBufferInfo = twgl.primitives.createSphereBufferInfo(gl, 1, 20, 20);
    this.sphereBufferInfo.attribs.aPosition = this.sphereBufferInfo.attribs.position;

    // plane
    this.planeBufferInfo = twgl.primitives.createPlaneBufferInfo(gl, 10, 10, 20, 20);
    this.planeBufferInfo.attribs.aPosition = this.planeBufferInfo.attribs.position;

    // torus
    this.torusBufferInfo = twgl.primitives.createTorusBufferInfo(gl, 1, .2, 32, 32);
    this.torusBufferInfo.attribs.aPosition = this.torusBufferInfo.attribs.position;
    requestAnimationFrame(this.Update);
    requestAnimationFrame(this.Render);
  }

  HandleKeyPress(event: KeyboardEvent) {
    switch(event.key)  {
      case 't':
        this.showFPS = !this.showFPS;
        break;
      case 'g':
      case 'G':
        this.showGrid = !this.showGrid;
    }
  }

  HandleDrag(event) {
    if(this.mouseDown === true){
      this.cameraRot += event.movementX *  .01;
      this.cameraRotUp += event.movementY  * .1;
      let clamp = 20;
      this.cameraRotUp = this.cameraRotUp > clamp ? clamp : this.cameraRotUp;
      this.cameraRotUp = this.cameraRotUp < -clamp ? -clamp : this.cameraRotUp;
    }
  }

  Update(time: number){
    const gl = this.gl;

    // Hot-reload shader
    this.progDrawColorUnif = twgl.createProgramInfo(gl, [passThruTransform, drawColorUnif]);

    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.dt = time - IndexPage.prevTime; 
    this.fps = 1000 / this.dt;
    
    IndexPage.prevTime = time;
    if (this.showFPS){
      this.fpsDiv.innerText = `${this.fps.toFixed(1)}`;
      this.fpsDiv.style = "font-size: 100px; text-align:right";
    }else{
      this.fpsDiv.innerHTML = "";
    }

    requestAnimationFrame(this.Update);
  }

  Render(time: number){
    // https://twgljs.org/examples/twgl-cube.html

    const gl = this.gl;
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    const fov = this.fov * Math.PI / 180;
    const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    const zNear = 0.5;
    const zFar = 1000;
    const projection = m4.perspective(fov, aspect, zNear, zFar);

    let eye = v3.create(0, this.cameraRotUp, -8);
    eye = v3.normalize(eye);
    eye = v3.mulScalar(eye, 10);
    const target = [0, 0, 0];
    let up = [0, 1, 0];

    let camera = m4.lookAt(eye, target, up);
    let view = m4.inverse(camera);
    view = m4.axisRotate(view, [0,1,0], this.cameraRot);    
    
    let viewProjection = m4.multiply(projection, view);
    let world = m4.rotationY(0);     
    world = m4.translate(world, v3.create(-4,0,0));
    world = m4.rotateY(world, time * 0.001);

    let uniforms = {
      uColor: [ 1, .5, 0, 1 ],
      uMVP: m4.multiply(viewProjection, world),
    };

    gl.useProgram(this.progDrawColorUnif.program);
    twgl.setBuffersAndAttributes(gl, this.progDrawColorUnif, this.bufferInfo);
    twgl.setUniforms(this.progDrawColorUnif, uniforms);
    gl.drawElements(gl.TRIANGLES, this.bufferInfo.numElements, gl.UNSIGNED_SHORT, 0);   
    
    // CYLINDER
    camera = m4.lookAt(eye, target, up);
    viewProjection = m4.multiply(projection, view);
    world = m4.rotationX(Math.PI / 2); 
    world = m4.translate(world, v3.create(4,0,0));    
    world = m4.rotateZ(world, -time * 0.001);

    uniforms = {
      uColor: [ 1, 1, 0, 1 ],
      uMVP: m4.multiply(viewProjection, world), 
    };

    twgl.setBuffersAndAttributes(gl, this.progDrawColorUnif, this.cylinderBufferInfo);
    twgl.setUniforms(this.progDrawColorUnif, uniforms);
    gl.drawElements(gl.TRIANGLES, this.cylinderBufferInfo.numElements, gl.UNSIGNED_SHORT, 0);

    // TEAPOT
    camera = m4.lookAt(eye, target, up);
    viewProjection = m4.multiply(projection, view);
    world = m4.rotationY(0);    
    world = m4.translate(world, v3.create(0,0,0));
    world = m4.rotateY(world, time * 0.001);
    world = m4.scale(world, [.025,.025,.025]);

    uniforms = {
      uColor: [ 0, 1, 1, 1 ],
      uMVP: m4.multiply(viewProjection, world), 
    };

    twgl.setBuffersAndAttributes(gl, this.progDrawColorUnif, this.teapotBufferInfo);
    twgl.setUniforms(this.progDrawColorUnif, uniforms);
    gl.drawElements(gl.TRIANGLES, this.teapotObj.indexBuffer.numItems, gl.UNSIGNED_SHORT, 0);  

    // SPHERE
    camera = m4.lookAt(eye, target, up);
    
    viewProjection = m4.multiply(projection, view);
    world = m4.rotationY(0);     
    world = m4.translate(world, v3.create(0, 0, 4));
    world = m4.rotateY(world, time * 0.001);

    uniforms = {
      uColor: [ 1, 0, 1, 1 ],
      uMVP: m4.multiply(viewProjection, world), 
    };

    twgl.setBuffersAndAttributes(gl, this.progDrawColorUnif, this.sphereBufferInfo);
    twgl.setUniforms(this.progDrawColorUnif, uniforms);
    gl.drawElements(gl.TRIANGLES, this.sphereBufferInfo.numElements, gl.UNSIGNED_SHORT, 0);

    if (this.showGrid){
      // PLANE
      camera = m4.lookAt(eye, target, up);
      viewProjection = m4.multiply(projection, view);
      world = m4.rotationY(0); 

      uniforms = {
        uColor: [ 0, 1, 0, 1 ],
        uMVP: m4.multiply(viewProjection, world), 
      };

      twgl.setBuffersAndAttributes(gl, this.progDrawColorUnif, this.planeBufferInfo);
      twgl.setUniforms(this.progDrawColorUnif, uniforms);
      gl.drawElements(gl.LINES, this.planeBufferInfo.numElements, gl.UNSIGNED_SHORT, 0);
    }

    // TORUS
    camera = m4.lookAt(eye, target, up);
    viewProjection = m4.multiply(projection, view);
    world = m4.rotationY(0); 

    world = m4.translate(world, v3.create(0, 0, -4));
    world = m4.rotateX(world, Math.PI / 2);
    world = m4.rotateZ(world, time * -0.001);

    uniforms = {
      uColor: [ .5, .5, .5, 1 ],
      uMVP: m4.multiply(viewProjection, world), 
    };

    twgl.setBuffersAndAttributes(gl, this.progDrawColorUnif, this.torusBufferInfo);
    twgl.setUniforms(this.progDrawColorUnif, uniforms);
    gl.drawElements(gl.TRIANGLES, this.torusBufferInfo.numElements, gl.UNSIGNED_SHORT, 0);

    requestAnimationFrame(this.Render);
  }

  AdjustFov(degrees){
    this.fov += degrees * .05; 
  }

  HandleScroll(event: MouseWheelEvent){
    this.AdjustFov(event.deltaY);
  }

  render(){
    return(
      <div>
        <canvas onMouseMove={this.HandleDrag} onMouseDown={(e) =>{ if(e.button === 0) this.mouseDown = true;}} onMouseUp={(e)=>{if(e.button === 0) this.mouseDown = false;}} id="canvas" ref= {(myRef) => {this.canvas = myRef;}} style={{position:"fixed"}}/>
        <Image/>
        <div ref= {(myRef) => {this.fpsDiv = myRef}} ></div>
      </div>
    );
  }
}

export default IndexPage;
