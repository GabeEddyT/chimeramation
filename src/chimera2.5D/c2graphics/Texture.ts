import {GraphicsObjectHandle} from "./GraphicsObjectHandle"

// header

// A3: OpenGL 2D texture descriptor.
//	member handle: graphics object handle
//	members width, height: the dimensions of the image
//	member channels: the number of channels per pixel
//	member bytes: the number of bytes per pixel
//	member internalFormat: OpenGL texture format flag
//	member internalType: OpenGL type flag
export class Texture{
    handle: GraphicsObjectHandle;
    width: number;
    height: number;
    channels: number; 
    bytes: number;
    internalFormat: number; 
    internalType: number;
}

// A3: Structure with details about a pixel format from a given type.
//	member internalFormat: describes the channel arrangement
//	member internalFormatBits: channel arrangement and size flag
//	member internalDataType: OpenGL data type flag for channels
//	member channelsPerPixel: how many color channels per pixel
//	member bytesPerChannel: how many bytes per channel
export class PixelFormatDescriptor{
    internalFormat: number;
    internalFormatBits: number;
    internalDataType: number;
    channelsPerPixel: number;
    bytesPerChannel: number;
};

// A3: Format of texture pixel.
export enum PixelType{
    // single channel
    r8,				// 8 bits per channel (byte)
    r16,				// 16 bits per channel (short)
    r32F,				// 32 bits per channel (float)

    // two channels
    rg8,
    rg16,
    rg32F,

    // 3-channel red-green-blue
    rgb8,
    rgb16,
    rgb32F,

    // 4-channel red-green-blue-alpha
    rgba8,
    rgba16,
    rgba32F,

    // luminance and depth textures
    luminance8,		// 8-bit luminance texture
    luminance16,		// 16-bit luminance
    depth16,			// 16-bit depth texture
    depth24,			// 24-bit depth
    depth32,			// 32-bit depth (a3i32)
    depth24_stencil8,	// 24-bit depth with 8-bit stencil
};

// A3: Texture unit for activating textures.
export enum Unit{
    unit00,
    unit01,
    unit02,
    unit03,
    unit04,
    unit05,
    unit06,
    unit07,
    unit08,
    unit09,
    unit10,
    unit11,
    unit12,
    unit13,
    unit14,
    unit15,
};

// A3: Maximum texture units available.
export const unitMax = 16;

// A3: Texture setting options for filter.
export enum FilterOption
{
    Nearest,	// magnified pixels clamp to nearest neighbor
    Linear,		// magnified pixels blend with neighbors
};

// A3: Texture setting options for repeating textures.
export enum RepeatOption
{
    Clamp,		// texture does not repeat; edge pixels stretch
    Mirror,		// texture mirrors at edges
    Normal,		// texture repeats
};