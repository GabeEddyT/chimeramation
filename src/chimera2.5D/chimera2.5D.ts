import {BufferObject} from "./c2graphics/BufferObject";
import * as handle from "./c2graphics/GraphicsObjectHandle"
import * as texture from "./c2graphics/Texture"

// const BufferObject = buffer.BufferObject;
const GraphicsObjectHandle = handle.GraphicsObjectHandle;
const Texture = texture.Texture;
const tex_pt = texture.PixelType;
const tex_u = texture.Unit;
const tex_ro = texture.RepeatOption;

// WebGL
// if (document.getElementsByTagName("canvas").length == 0) document.body.appendChild(document.createElement("canvas"));
// const canvas = <HTMLCanvasElement>window.("canvas")[0];
// export const gl = canvas.getContext("webgl2");

export default{
    // gl,
    BufferObject,
    handle, 
    GraphicsObjectHandle,
    texture,
    Texture,
    tex_pt,
    tex_u,
    tex_ro
}