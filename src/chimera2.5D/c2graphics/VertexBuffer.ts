import {gl} from '../chimera2.5D';
import BufferObject from '../c2graphics/BufferObject'
import { GraphicsObjectHandle } from './GraphicsObjectHandle';

export enum VertexAttributeName{
    // defaults, matching OpenGL's specification
    a3attrib_position,
    a3attrib_blendWeights,
    a3attrib_normal,
    a3attrib_color,
    a3attrib_colorSecondary,
    a3attrib_fogCoord,
    a3attrib_pointSize,
    a3attrib_blendIndices,
    a3attrib_texcoord0,
    a3attrib_texcoord1,
    a3attrib_texcoord2,
    a3attrib_texcoord3,
    a3attrib_texcoord4,
    a3attrib_texcoord5,
    a3attrib_texcoord6,
    a3attrib_texcoord7,

    // alternative names
    a3attrib_texcoord = a3attrib_texcoord0,
    a3attrib_texcoordSecondary,
    a3attrib_tangent,
    a3attrib_bitangent,
    a3attrib_positionSecondary,
    a3attrib_normalSecondary,
    a3attrib_tangentSecondary,
    a3attrib_bitangentSecondary,

    // user alternatives
    a3attrib_user00 = a3attrib_position,
    a3attrib_user01,
    a3attrib_user02,
    a3attrib_user03,
    a3attrib_user04,
    a3attrib_user05,
    a3attrib_user06,
    a3attrib_user07,
    a3attrib_user08,
    a3attrib_user09,
    a3attrib_user10,
    a3attrib_user11,
    a3attrib_user12,
    a3attrib_user13,
    a3attrib_user14,
    a3attrib_user15,
};

export class VertexAttributeDataDescriptor{
    name: VertexAttributeName;
    data: any;
};

const nameMax = 16;

export class VertexBuffer extends BufferObject{
    VertexBufferStore(vertexFormat: VertexFormatDescriptor, attribRawData: VertexAttributeDataDescriptor,  vertexCount: number, offset_out_opt: [number]){
        let ret = 0;
        let storage: number;
        let attribIndex: number, vertexIndex: number;
        let interleaved: any, interleavedPtr: any;
        let attribData: Uint8Array, attribDataPtr: any;
        let attribSize: number;

        // ALGORITHM: 
        //	- generate CPU-side array to hold the data before shipping to GPU
        //	- for each attribute...
        //		- for each element in this attribute...
        //			- insert into our giant array
        //			***e.g. if element is a vector, insert 2/3/4 items (we are assuming 3)
        //	- stuff GPU-side VBO with raw data in our array!
        //	- free our CPU-side memory allocation

        // PURPOSE: 
        // these are not definitely the attributes in-use, but we'll say 
        //	we are using positions, normals, texcoords: 
        //	positions(cpu) = |pos0|pos1|pos2|pos3|...|
        //	normals(cpu)   = |nrm0|nrm1|nrm2|nrm3|...|
        //	texcoords(cpu) = |tex0|tex1|tex2|tex3|...|
        // 
        // take this data and interleave it so attributes with the same number
        //	are side-by-side: 
        //	interleavedData(cpu) = |pos0|nrm0|tex0|pos1|nrm1|tex1|pos2|nrm2|tex2|pos3|nrm3|tex3|...|...|...|...|
        // contiguous memory means faster access for drawing!

        // validate params
        if (this && vertexFormat && attribData && vertexCount)
        {
            // validate initialized
            if (this.handle.handle)
            {
                // validate other params
                if (vertexFormat.vertexNumAttribs)
                {
                    // copy attribute data passed in to internal container
                    // use vertex descriptor to dictate how many there are
                    for (attribIndex = 0; attribIndex < vertexFormat.vertexNumAttribs; ++attribIndex)
                        attribData[attribRawData[attribIndex].name] = attribRawData[attribIndex].data;

                    // confirm that all attributes have matching data
                    for (attribIndex = 0; attribIndex < nameMax; ++attribIndex)
                        if (vertexFormat.attribType[attribIndex] && !attribData[attribIndex])
                            break;

                    // if loop completed, all attributes have data
                    if (attribIndex == nameMax)
                    {
                        // check if interleaved data will fit in buffer
                        storage = vertexFormat.StorageSpaceRequired(vertexCount);
                        if ( this.ValidateBlockSize(0, storage) > 0)
                        {
                            // allocate interleaved data array
                            interleaved = new ArrayBuffer(storage);

                            // for each attribute, copy instances in steps
                            for (attribIndex = 0; attribIndex < nameMax; ++attribIndex)
                            {
                                // vertex is used
                                if (vertexFormat.attribType[attribIndex])
                                {
                                    interleavedPtr = interleaved + vertexFormat.attribOffset[attribIndex];
                                    attribDataPtr = attribData[attribIndex];
                                    attribSize = vertexFormat.attribSize[attribIndex];
                                    for (vertexIndex = 0; vertexIndex < vertexCount; ++vertexIndex)
                                    {
                                        
                                        // memcpy(interleavedPtr, attribDataPtr, attribSize);
                                        interleavedPtr = new ArrayBuffer(attribDataPtr);
                                        interleavedPtr += vertexFormat.vertexSize;
                                        attribDataPtr += attribSize;
                                    }
                                }
                            }

                            // fill buffer (should definitely return positive 
                            //	after all the checks done previously)
                            ret = this.Fill(0, storage, interleaved, offset_out_opt);
                            
                            if (ret <= 0)
                                console.log("\n A3 ERROR (VBO %u '%s'): \n\t Vertex data not stored in buffer.", this.handle.handle, this.handle.name);

                            // release interleaved data
                            // free(interleaved);
                        }
                        else
                        console.log("\n A3 ERROR (VBO %d '%s'): \n\t Vertex data will not fit in buffer.", this.handle.handle, this.handle.name);
                    }
                    else
                    console.log("\n A3 ERROR (VBO %d '%s'): \n\t Vertex attribute (%u) has no data.", this.handle.handle, this.handle.name, attribIndex);
                }
                else
                     console.log("\n A3 ERROR (VBO %d '%s'): \n\t Vertex descriptor has no attributes.", this.handle.handle, this.handle.name);

                // done
                return ret;
            }
        }
        return -1;
    }

};
type IndexBuffer = BufferObject;

class VertexArrayDescriptor{
    handle: GraphicsObjectHandle;
    vertexFormat: VertexFormatDescriptor;
    vertexBuffer: VertexBuffer;
}

function pad(n: number, p: number) {return ( (n) + ( (p) - (n) % (p) ) % (p) );};
function pad4(n: number){ return pad(n,4);};

class VertexFormatDescriptor{
    attribType: Uint16Array = new Uint16Array(nameMax);
    attribOffset: Uint16Array = new Uint16Array(nameMax);
    attribElements: Uint8Array = new Uint8Array(nameMax);
    attribSize: Uint8Array = new Uint8Array(nameMax);
    vertexSize: number;
    vertexNumAttribs: number;

    StorageSpaceRequired(count: number){
        if (this){
		    // padding may be required
            const bytes = (this.vertexSize * count);
            return pad4(bytes);
        }
	    return -1;
    }
};